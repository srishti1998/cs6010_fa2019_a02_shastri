# TABLE OF CONTENTS
[TECHNOLOGIES USED](#technology-used)

[QUESTIONS](#questions)


## TECHNOLOGIES USED

> **Jupyter lab**

It is a open source web application used for presenting data science projects. It is interactive user-interface which allows us to create and share documents and offers high performance.
I used jupyterlab to extract data from the given website. I wrote the code in different cells and also used markups and comment codes. At last the final cell contain polished code that achieves the main goal of assignment. 

> **Pandas**

It is a high performance and easy to use python library used for analysis, importing, visualization  and manipulation of data. I used python pandas library for extracting the data and stored the extracted information in pandas dataframe.

> **Anaconda Navigator**

its is a desktop graphical user interface which allows to launch applications such as jupyterlab, jupyternotebook, VSCode etc. It also easily manages the packages and environments.

> **Cookiecutter**

 It is a project template to build a python project and it can be in any programming language.

> **Gitlab**

It is a open source repository which includes version control and allows you to decide whether you want it public, private or free. I created a new project in gitlab cs6010_fa2019_a02_shastri and pushed all my files there.

## QUESTIONS

> Q	What did I do well in this assignment?

I created virtual environment easily and obtained the the data from the website http://quotes.toscrape.com/.

> Q	What was the most challenging part of this assignment?

The most challenging part of this assignment is to use cookiecutter to generate appropriate directory structure.
> Q  How can I improve on things that were very challenging to me?

I can improve the things that were challenging to me by practicing more on that and by designing more projects using cookiecutter templates.

> Q What do I need help with?
I need help with how to use cookiecutter templates.

